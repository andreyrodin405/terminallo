#include "settingswindow.h"
#include "ui_settingswindow.h"

Settingswindow::Settingswindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settingswindow)
{
    ui->setupUi(this);

    QCommonStyle style;
    ui->port_button->setIcon(style.standardIcon(QStyle::SP_DialogOpenButton));
}

Settingswindow::~Settingswindow()
{
    delete ui;
}

void Settingswindow::on_buttonBox_accepted()
{
    this->close();
    emit enable_mainWindow(true);
}

void Settingswindow::on_buttonBox_rejected()
{
    this->close();
    emit enable_mainWindow(true);
}

void Settingswindow::on_Settingswindow_finished(int result)
{
    (void) result; // suppress warnings
    emit enable_mainWindow(true);
}

void Settingswindow::on_port_button_clicked()
{
    //QString get_path = QFileDialog::getOpenFileName(0,"Choose port device","/dev/");
    QString get_path = QFileDialog::getOpenFileName(this,"Choose port device","/dev","tty*");
}
