#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "settingswindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_SettingsButton_clicked();

    void on_ClearButton_clicked();

    void on_AboutButton_clicked();

    void on_CloseButton_clicked();

    void on_ConnectionStateButton_clicked();

    void on_EnterButton_clicked();

private:
    Ui::MainWindow *ui;

    Settingswindow * Settingswind;
};

#endif // MAINWINDOW_H
