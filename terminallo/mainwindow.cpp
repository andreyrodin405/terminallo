#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Инициализируем окно настроек
    Settingswind = new Settingswindow();

    // подключаем к слоту запуска главного окна по кнопке в окне настроек
    connect(Settingswind, &Settingswindow::enable_mainWindow, this, &MainWindow::setEnabled);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_SettingsButton_clicked()
{
    Settingswind->show(); // показываем окно настроек
    this->setDisabled(true); // парализуем главное окно


}

void MainWindow::on_ClearButton_clicked()
{

}

void MainWindow::on_AboutButton_clicked()
{

}

void MainWindow::on_CloseButton_clicked()
{

}

void MainWindow::on_ConnectionStateButton_clicked()
{

}

void MainWindow::on_EnterButton_clicked()
{

}
