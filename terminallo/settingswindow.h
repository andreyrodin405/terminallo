#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>
#include <QFileDialog>
#include <QIcon>
#include <QCommonStyle>

namespace Ui {
class Settingswindow;
}

class Settingswindow : public QDialog
{
    Q_OBJECT

public:
    explicit Settingswindow(QWidget *parent = 0);
    QIcon * port_button_icon; // для иконки папки
    ~Settingswindow();

signals:
    void enable_mainWindow(bool value);  // Сигнал для главного окна на открытие

private slots:

    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_Settingswindow_finished(int result);

    void on_port_button_clicked();

private:


    Ui::Settingswindow *ui;
};

#endif // SETTINGSWINDOW_H
